using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestData : MonoBehaviour
{
    public string UIText;
    public GameObject LightsContainer;
    public GameObject ItemContainer;

    public bool Completed = false;

    private void Start()
    {
        QuestManager.Instance.Register(this);
        gameObject.SetActive(false);
    }

    void OnEnable()
    {
        LightsContainer.SetActive(true);
        ItemContainer.SetActive(true);
    }
    void OnDisable()
    {
        LightsContainer.SetActive(true);
        ItemContainer.SetActive(true);
    }
}
