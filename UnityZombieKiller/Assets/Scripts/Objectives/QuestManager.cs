using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    private List<QuestData> m_quests;
    public int NumQuests = 3;

    public List<QuestData> SelectedQuests;

    public static QuestManager Instance
    {
        get;
        private set;
    }

    public void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        SelectedQuests = new List<QuestData>();
        while (SelectedQuests.Count != NumQuests)
        {
            int index = Random.Range(0, m_quests.Count);

            if (!SelectedQuests.Contains(m_quests[index]))
            {
                SelectedQuests.Add(m_quests[index]);
                m_quests[index].gameObject.SetActive(true);
            }
        }
    }

    public void Register(QuestData questData)
    {
        if(m_quests == null)
            m_quests = new List<QuestData>();
        m_quests.Add(questData);
    }
}
