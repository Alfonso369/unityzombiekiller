using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BodyPart
{
    HEAD,
    BODY
};

public class BodyPartID : MonoBehaviour
{
    public BodyPart Part;
    public ZombieAIBehaviour ZombieReference;
}
