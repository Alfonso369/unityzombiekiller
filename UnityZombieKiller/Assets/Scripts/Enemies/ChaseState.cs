﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChaseState : State
{
    private Animator m_animator;
    private Transform m_playerTransform;
    private Vector3 m_lastKnownPosition;
    private NavMeshAgent m_navMeshAgent;
    //private float m_elapsedTime = 0f;
    public float ChaseTimeOut = 2f;

    void Awake()
    {
        m_animator = GetComponent<Animator>();
        m_navMeshAgent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        m_playerTransform = GameManager.Instance.GetPlayer().transform;
        m_lastKnownPosition = m_playerTransform.position;
        //m_elapsedTime = 0;
    }

    public override void Enter()
    {
        m_navMeshAgent.updateRotation = true;
        m_navMeshAgent.isStopped = false;

        m_animator.SetFloat("Speed", m_character.RunSpeed);

        m_navMeshAgent.speed = m_character.RunSpeed;
    }

    public override void Exit()
    {
        m_navMeshAgent.updateRotation = false;
        m_navMeshAgent.isStopped = true;
        m_animator.SetFloat("Speed", 0);
    }

    public void Update()
    {
        m_character.UpdateRamdomSounds();

        //if (!m_character.CanSeePlayer() && !m_character.LifeChanged() && !m_character.CanHearPlayer())
        //{
        //    Debug.Log(Vector3.Distance(transform.position, m_lastKnownPosition));
        //    if(Vector3.Distance(transform.position, m_lastKnownPosition) < 2)
        //    {
        //        m_animator.SetFloat("Speed", 0);
        //        m_animator.SetTrigger("Idle");
        //        m_elapsedTime += Time.deltaTime;

        //        if (m_elapsedTime >= ChaseTimeOut)
        //        {
        //            m_elapsedTime = 0;
        //            m_stateMachine.ChangeState(m_character.m_idle);
        //        }
        //    }
        //    else
        //    {
        //        m_animator.SetFloat("Speed", m_character.RunSpeed);
        //        m_navMeshAgent.SetDestination(m_lastKnownPosition);
        //    }
        //}
        //else
        //{
            //m_animator.SetFloat("Speed", m_character.RunSpeed);
            m_lastKnownPosition = m_playerTransform.position;
            m_navMeshAgent.SetDestination(m_lastKnownPosition);
            //m_elapsedTime = 0;
        //}

        if (Vector3.Distance(transform.position, m_playerTransform.position) <= m_character.AttackRange)
        {
            m_stateMachine.ChangeState(m_character.m_attack);
        }

        if (m_character.IsDead())
        {
            m_stateMachine.ChangeState(m_character.m_dead);
        }
    }
}
