﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponTrigger  : TriggerObject
{
    float m_damage;

    public void Init(float _damage)
    {
        m_damage = _damage;
    }

    public override void OnTriggerWithPlayer(Player player)
    {
        player.TakeDamage(m_damage);
        EnableCollider(false);
    }

    public void EnableCollider(bool enable)
    {
        GetComponent<SphereCollider>().enabled = enable;
    }
}

