﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WanderState : State
{
    private Animator  m_animator;
    private NavMeshAgent  m_navMeshAgent;

    public float WalkRadius = 5f;

    private Vector3 m_targetPosition;
    private bool m_wandering = false;

    private Vector3 m_startingPosition;

    void Awake()
    {
        m_animator = GetComponent<Animator>();
        m_navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public override void Enter()
    {
        m_navMeshAgent.updateRotation = true;
        m_navMeshAgent.isStopped = false;

        m_wandering = false;
        m_startingPosition = transform.position;
        m_animator.SetFloat("Speed", m_character.WalkSpeed);
        m_animator.SetInteger("RandomAnimation", Random.Range(0, 2));
    }

    public override void Exit()
    {
        m_navMeshAgent.updateRotation = false;
        m_navMeshAgent.isStopped = true;

        m_animator.SetFloat("Speed", 0);
    }

    public void Update()
    {
        m_character.UpdateRamdomSounds();

        if(!m_wandering)
        {
            Vector3 randomDirection = Random.insideUnitSphere * WalkRadius;
            randomDirection += m_startingPosition;
            NavMeshHit hit;
            NavMesh.SamplePosition(randomDirection, out hit, WalkRadius, 1);
            m_targetPosition = hit.position;
            m_navMeshAgent.SetDestination(m_targetPosition);
            m_navMeshAgent.speed = m_character.WalkSpeed;

            m_wandering = true;
        }
        else
        {
            if(Vector3.Distance(transform.position, m_targetPosition) < 0.1f)
            {
                m_stateMachine.ChangeState(m_character.m_idle);
            }
        }

        if (m_character.CanSeePlayer() || m_character.LifeChanged() || m_character.CanHearPlayer())
        {
            m_stateMachine.ChangeState(m_character.m_chase);
        }

        if (m_character.IsDead())
        {
            m_stateMachine.ChangeState(m_character.m_dead);
        }
    }
}
