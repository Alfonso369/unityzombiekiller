﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IdleState : State
{
    private Animator  m_animator;

    public float MinTimeInPoint = 2;
    public float MaxTimeInPoint = 5;

    private float m_timeInPoint;

    private float m_timerInPoint = 0;

    void Awake()
    {
        m_animator = GetComponent<Animator>();
    }

    public override void Enter()
    {
        m_animator.SetTrigger("Idle");
        m_animator.SetFloat("Speed", 0);

        m_timeInPoint = Random.Range(MinTimeInPoint, MaxTimeInPoint);

    }

    public override void Exit()
    {
        m_animator.ResetTrigger("Idle");
    }

    public void Update()
    {
        m_character.UpdateRamdomSounds();

        m_timerInPoint += Time.deltaTime;

        if(m_timerInPoint > m_timeInPoint)
        {
            m_timerInPoint = 0;
            m_stateMachine.ChangeState(m_character.m_wander);
        }

        if (m_character.CanSeePlayer() || m_character.LifeChanged() || m_character.CanHearPlayer())
        {
            m_stateMachine.ChangeState(m_character.m_chase);
        }

        if (m_character.IsDead())
        {
            m_stateMachine.ChangeState(m_character.m_dead);
        }
    }
}
