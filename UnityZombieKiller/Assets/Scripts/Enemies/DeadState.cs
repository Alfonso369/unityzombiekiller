﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DeadState : State
{
    private Animator  m_animator;
    private NavMeshAgent m_navMeshAgent;

    void Awake()
    {
        m_animator = GetComponent<Animator>();
        m_navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public override void Enter()
    {
        m_navMeshAgent.isStopped = true;
        m_navMeshAgent.updateRotation = false;
        m_animator.SetTrigger("Dead");
        m_animator.SetInteger("RandomAnimation", Random.Range(0, 2));
        m_navMeshAgent.enabled = false;
        m_character.enabled = false;
        enabled = false;

        GameManager.Instance.RemoveZombie();
    }

    public override void Exit()
    {
    }

    public void Update()
    {
    }
}
