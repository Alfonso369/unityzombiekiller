using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieAIBehaviour : MonoBehaviour
{
    [Header("Stats")]
    public float MaxHealth = 100f;
    public float Health = 100f;
    public float Damage = 10f;
    public float WalkSpeed = 1f;
    public float RunSpeed = 4f;
    public float AttackRange = 2f;
    public float HeadDamageMultiplier = 2f;

    [Header("Sounds")]
    public AudioClip[] IdleSounds;
    public AudioClip AttackSound;
    public float MinTimeIdleSounds = 0;
    public float MaxTimeIdleSounds = 1;

    StateMachine movementSM;
    [Header("State Machine")]
    public WeaponTrigger m_rightHandTrigger;
    public WeaponTrigger m_leftHandTrigger;
    public IdleState m_idle;
    public WanderState m_wander;
    public ChaseState m_chase;
    public AttackState m_attack;
    public DeadState m_dead;

    [Header("Senses - Sight")]
    public float FieldOfView = 60;
    public float SightDistance = 30;

    [Header("Senses - Hearing")]
    public float HearingRange = 60;

    private Transform m_playerTransform;
    private float m_randomTimeSound = 0;
    private float m_countTimeSound = 0;
    private bool m_lifeChanged = false;
    private bool m_heardPlayer = false;

    public void TakeDamage(float _damage, BodyPart _bodyPart)
    {
        if(_bodyPart == BodyPart.HEAD)
        {
            Health = Mathf.Clamp(Health - Mathf.Abs(_damage * HeadDamageMultiplier), 0, MaxHealth);
        }
        else
        {
            Health = Mathf.Clamp(Health - Mathf.Abs(_damage), 0, MaxHealth);
        }
        m_lifeChanged = true;
        //Debug.Log(Health);
    }

    private void Start()
    {
        GameManager.Instance.AddZombie();
        m_playerTransform = GameManager.Instance.GetPlayer().transform;

        movementSM = GetComponent<StateMachine>();

        m_idle = GetComponent<IdleState>();
        m_wander = GetComponent<WanderState>();
        m_attack = GetComponent<AttackState>();
        m_chase = GetComponent<ChaseState>();
        m_dead = GetComponent<DeadState>();

        //m_patrol.Init(this, movementSM);
        m_idle.Init(this, movementSM);
        m_wander.Init(this, movementSM);
        m_attack.Init(this, movementSM);
        m_chase.Init(this, movementSM);
        m_dead.Init(this, movementSM);

        if(m_rightHandTrigger)
            m_rightHandTrigger.Init(Damage);
        if(m_leftHandTrigger)
            m_leftHandTrigger.Init(Damage);

        m_randomTimeSound = Random.Range(MinTimeIdleSounds, MaxTimeIdleSounds);

        movementSM.Init(m_idle);

        InputManager.Instance.OnPlayerShooting += PlayerShootHear;
    }

    public void PlayerShootHear()
    {
        m_heardPlayer = true;
    }

    public void StartRightAttack ()
    {
        m_rightHandTrigger.EnableCollider(true);
        PlayAttackSound();
    }

    public void FinishRightAttack ()
    {
        m_rightHandTrigger.EnableCollider(false);
    }

    public void StartLeftAttack ()
    {
        m_leftHandTrigger.EnableCollider(true);
    }

    public void FinishLeftAttack ()
    {
        m_leftHandTrigger.EnableCollider(false);
    }

    public bool CanSeePlayer()
    {
        float Angle = Mathf.Abs(Vector3.Angle(transform.forward, (m_playerTransform.position - transform.position).normalized));

        if (Angle > FieldOfView)
            return false;

        if(Vector3.Distance(m_playerTransform.position, transform.position) > SightDistance)
            return false;

        return true;
    }

    public bool CanHearPlayer()
    {
        if(m_heardPlayer)
        {
            m_heardPlayer = false;
            if(Vector3.Distance(m_playerTransform.position, transform.position) > HearingRange)
                return false;

            return true;
        }
        return false;
    }

    public bool LifeChanged()
    {
        if(m_lifeChanged)
        {
            m_lifeChanged = false;
            return true;
        }
        return false;
    }

    public bool IsDead()
    {
        return Health == 0;
    }

    public void UpdateRamdomSounds()
    {
        m_countTimeSound += Time.deltaTime;
        if(m_countTimeSound > m_randomTimeSound)
        {
            int random = Random.Range(0, IdleSounds.Length);
            GetComponent<AudioSource>().PlayOneShot(IdleSounds[random]);

            m_randomTimeSound = Random.Range(MinTimeIdleSounds, MaxTimeIdleSounds);
            m_countTimeSound = 0;
        }
    }

    public void PlayAttackSound()
    {
        GetComponent<AudioSource>().PlayOneShot(AttackSound);
    }

    private void OnDrawGizmos()
    {
        /*Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawSphere(transform.position, SightDistance);

        Gizmos.color = new Color(1, 1, 1, 0.5f);
        Gizmos.DrawSphere(transform.position, HearingRange);*/

        Gizmos.DrawIcon(transform.position + new Vector3(0, 2, 0), "BuildSettings.Lumin");
    }
}
