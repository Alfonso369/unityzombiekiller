using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    public Vector3 RotationAxis;
    public float RotationSpeed;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(RotationAxis, RotationSpeed * Time.deltaTime);
    }
}
