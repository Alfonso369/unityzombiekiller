using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleCamera : MonoBehaviour
{
    public Camera PlayerCamera;

    private float m_initialDist;

    void Start()
    {
        //m_initialDist = Vector3.Distance(transform.position, PlayerCamera.transform.position);
    }

    void Update()
    {
        //transform.rotation = PlayerCamera.transform.rotation;

        float dist = Vector3.Distance(transform.position, PlayerCamera.transform.position);
        transform.localScale = Vector3.one * dist / 10;
    }
}
