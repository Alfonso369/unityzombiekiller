﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PistolType
{
	None,
	Glock,
	AK47,
	Shotgun
}

public class VerySimplePistol : MonoBehaviour
{
	public Transform RaycastSpot;
	public Texture2D CrosshairTexture;
    public AudioClip FireSound;
    public AudioClip ReloadSound;
	public ParticleSystem ParticleSystem;
	public Animation ReloadAnimation;
	public LayerMask RaycastLayerToIgnore;

	public Camera WeaponCamera;

	public PistolType Type;

	public bool ContinuousShooting = false;
	public int SimultaneousBullets = 1;

	public int BulletsPerMagazine = 50;
	public int RemainingBulletsInMagazine = 50;
	public int TotalRemainingBullets = 500;

	public float DamagePerBullet = 80.0f;
	public float ForceToApply = 20.0f;
	public float WeaponRange = 9999.0f;
	public float TimeBetweenShots = 0.5f;
	public float TimeToReload = 1.0f;

	public float NoiseRadius = 1;

	public int m_currentAccuracy = 100;
	public int m_accuracyDropPerShop = 5;

    private bool m_canShot = true;
    private bool m_reloading = false;

	private bool m_inputReceived = false;

	private float m_shotTimer = 0;
	private float m_reloadTimer = 0;

	public void WeaponTriggerPressed()
    {
		m_inputReceived = true;
    }

	public void WeaponTriggerReleased()
    {
		m_currentAccuracy = 100;
		m_inputReceived = false;
    }

	private void OnEnable()
    {
		m_shotTimer = 0;
		m_reloadTimer = 0;
		m_inputReceived = false;
		m_canShot = true;
    }

	private void Update()
	{
		if(m_shotTimer < TimeBetweenShots)
			m_shotTimer += Time.deltaTime;

		if(m_inputReceived)
        {
			if(!m_reloading)
            {
				if(m_canShot)
				{
					if(!ContinuousShooting)
                    {
						m_canShot = false;
                    }

					if(m_shotTimer >= TimeBetweenShots)
					{
						m_shotTimer = 0;
						Shot();
					}
				}

				if(!m_canShot)
					m_currentAccuracy = 100;

				if(RemainingBulletsInMagazine == 0)
				{
					m_currentAccuracy = 100;
					Reload();
				}
			}
		}
        else
        {
			m_canShot = true;
        }

		if(m_reloading)
        {
			if(m_reloadTimer < TimeToReload)
				m_reloadTimer += Time.deltaTime;
        }
	}

	private void OnGUI()
	{
		Vector2 center = new Vector2(Screen.width / 2, Screen.height / 2);
		Rect auxRect = new Rect(center.x - 20 / 2, center.y - 20 / 2, 20, 20);
		GUI.DrawTexture(auxRect, CrosshairTexture, ScaleMode.StretchToFill);
	}

	private void Shot()
	{
		if(RemainingBulletsInMagazine != 0)
        {
			int bulletsShoot = SimultaneousBullets;

			//Restar balas
			RemainingBulletsInMagazine-=SimultaneousBullets;
			if(RemainingBulletsInMagazine < 0)
            {
				bulletsShoot -= RemainingBulletsInMagazine;
				RemainingBulletsInMagazine = 0;
            }

			//Primero miramos en qué punto va a impactar la bala basándonos en el punto central de la cámara.
			Ray rayCam = new Ray(WeaponCamera.transform.position, WeaponCamera.transform.forward);

			RaycastHit hitCam;
			bool cameraHit = Physics.Raycast(rayCam, out hitCam, WeaponRange, ~RaycastLayerToIgnore);
			//Debug.Log("Hit " + hitCam.transform.name);
			Debug.DrawRay(WeaponCamera.transform.position, WeaponCamera.transform.forward * WeaponRange, Color.red, 4);


			for(int i = 0; i < bulletsShoot; i++)
            {
				float accuracyModifier = (100f - (float)m_currentAccuracy) / 1000f;

				Vector3 directionForward;
				
				if(!cameraHit)
                {
					directionForward = RaycastSpot.forward;
                }
				else
                {
					directionForward = (hitCam.point - RaycastSpot.position).normalized;
                }

				directionForward.x += UnityEngine.Random.Range(-accuracyModifier, accuracyModifier);
				directionForward.y += UnityEngine.Random.Range(-accuracyModifier, accuracyModifier);
				directionForward.z += UnityEngine.Random.Range(-accuracyModifier, accuracyModifier);
				m_currentAccuracy -= m_accuracyDropPerShop;
				m_currentAccuracy = Mathf.Clamp(m_currentAccuracy, 0 , 100);

				Ray ray = new Ray(RaycastSpot.position, directionForward);

				RaycastHit hit;

				if (Physics.Raycast(ray, out hit, WeaponRange, ~RaycastLayerToIgnore))
				{
					BodyPartID body = hit.transform.GetComponent<BodyPartID>();
					if(body)
                    {
						body.ZombieReference.TakeDamage(DamagePerBullet, body.Part);
                    }
				}
				Debug.DrawRay(RaycastSpot.position, directionForward * WeaponRange, Color.green, 4);
            }

			GetComponent<AudioSource>().PlayOneShot(FireSound);
			if(ParticleSystem)
            {
				ParticleSystem.gameObject.SetActive(true);
				ParticleSystem.Play();
            }
		}
	}

	public void Reload()
    {
		m_reloading = true;
		StartCoroutine(ReloadCoroutine());
    }

	private IEnumerator ReloadCoroutine()
    {
		ReloadAnimation.Play();
		yield return new WaitForSeconds(TimeToReload/2);

		GetComponent<AudioSource>().PlayOneShot(ReloadSound);

		yield return new WaitForSeconds(TimeToReload/2);


		if(TotalRemainingBullets > 0)
        {
			int usedBullets = BulletsPerMagazine - RemainingBulletsInMagazine;

			TotalRemainingBullets -= usedBullets;

			if(TotalRemainingBullets < 0)
            {
				usedBullets += TotalRemainingBullets;
				TotalRemainingBullets = 0;
            }

			RemainingBulletsInMagazine += usedBullets;
        }

		m_reloading = false;
		m_shotTimer = 0;
		m_reloadTimer = 0;
    }

	public float GetCurrentShootTimer()
    {
		return m_shotTimer;
    }

	public float GetCurrentReloadTimer()
    {
		return m_reloadTimer;
    }

	public void SetEnable(bool _enable)
    {
		gameObject.SetActive(_enable);
    }

	public bool IsEnabled()
    {
		return gameObject.activeInHierarchy;
    }

	public void AddAmmo(int _ammo)
    {
		TotalRemainingBullets += _ammo;
    }
}
