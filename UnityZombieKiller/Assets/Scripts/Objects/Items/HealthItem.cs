﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthItem : Item
{
    public int m_healthPerSecond;

    private float m_innerCount = 0;

    public override void OnTriggerWithPlayer(Player player)
    {
        base.OnTriggerWithPlayer(player);
        m_player.AddHealth(m_healthPerSecond);

        if(m_effect)
        {
            m_effect.SetActive(true);
        }

        if(m_sfx)
        {
            m_sfx.Play();
        }
    }

    public override void ExecuteAction(float _counterTime)
    {
        m_innerCount += Time.deltaTime;

        if(m_effect)
        {
            m_effect.transform.position = m_player.ItemEffectsSpot.transform.position;
        }

        if(m_innerCount > 1)
        {
            m_player.AddHealth(m_healthPerSecond);
            m_innerCount -= 1;
        }

        //Se hace al final para evitar que en el ultimo tick se termine la accion y no se ejecute AddHealth
        base.ExecuteAction(_counterTime);
    }

    public override void ExitAction()
    {
        m_effect.SetActive(false);
        base.ExitAction();
    }
}
