﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestItem : Item
{
    public QuestData QuestReference;

    public override void OnTriggerWithPlayer(Player player)
    {
        base.OnTriggerWithPlayer(player);
       
        QuestReference.Completed = true;
    }

    public override void ExecuteAction(float _counterTime)
    {
        base.ExecuteAction(_counterTime);
    }

    public override void ExitAction()
    {
        //m_effect.SetActive(false);
        base.ExitAction();
    }
}
