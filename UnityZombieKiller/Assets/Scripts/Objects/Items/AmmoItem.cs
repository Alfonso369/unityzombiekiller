﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoItem : Item
{
    public int Ammo;

    public PistolType AmmoType;

    public override void OnTriggerWithPlayer(Player player)
    {
        base.OnTriggerWithPlayer(player);
        m_player.AddAmmo(AmmoType, Ammo);

        if(m_sfx)
        {
            m_sfx.Play();
        }
    }

    public override void ExecuteAction(float _counterTime)
    {
        base.ExecuteAction(_counterTime);
    }

    public override void ExitAction()
    {
        base.ExitAction();
    }
}
