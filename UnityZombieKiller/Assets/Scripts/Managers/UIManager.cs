using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public Slider WeaponAmmoSlider;
    public Slider ReloadTimeSlider;
    public Slider CadencyTimeSlider;
    public Slider HealthSlider;
    public Slider StaminaSlider;
    public Text WeaponAmmoText;
    public Text HealthText;
    public TextMeshProUGUI ZombiesText;
    public GameObject DefeatUI;
    public GameObject WinUI;

    [Header("Quest")]
    public Text[] QuestTexts;
    public Toggle[] QuestToggles;

    public static UIManager Instance
    {
        get;
        private set;
    }

    public void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        Player player = GameManager.Instance.GetPlayer();

        if(player)
        {
            VerySimplePistol weapon = player.GetCurrentWeapon();
            if(WeaponAmmoSlider)
                WeaponAmmoSlider.value = ((float)weapon.RemainingBulletsInMagazine / (float)weapon.BulletsPerMagazine);
            if(WeaponAmmoText)
                WeaponAmmoText.text = weapon.RemainingBulletsInMagazine + "/" + weapon.BulletsPerMagazine + "\n" + weapon.TotalRemainingBullets;
            if(ReloadTimeSlider)
                ReloadTimeSlider.value = weapon.GetCurrentReloadTimer() / weapon.TimeToReload;
            if(CadencyTimeSlider)
                CadencyTimeSlider.value = weapon.GetCurrentShootTimer() / weapon.TimeBetweenShots;
            if(HealthSlider)
                HealthSlider.value = player.CurrentHealth / player.MaxHealth;
            if(HealthText)
                HealthText.text = player.CurrentHealth + "/" + player.MaxHealth;
            if(StaminaSlider)
                StaminaSlider.value = player.CurrentStamina / player.MaxStamina;
            if(ZombiesText)
                ZombiesText.text = ":" + GameManager.Instance.NumZombies;
            
            for(int i = 0; i <  QuestManager.Instance.SelectedQuests.Count; i++)
            {
                QuestToggles[i].isOn = QuestManager.Instance.SelectedQuests[i].Completed;
                QuestTexts[i].text = QuestManager.Instance.SelectedQuests[i].UIText;
            }
        }
    }

    public void ShowWinUI()
    {
        WinUI.SetActive(true);
    }

    public void ShowDefeatUI()
    {
        DefeatUI.SetActive(true);
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
