using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int NumZombies = 0;
    Player m_player;
    private int NumZombiesKilled = 0;
    private float AliveTime = 0;
    public delegate void OnVictory();
    public delegate void OnDefeat();

    public event OnVictory Victory;
    public event OnDefeat Defeat;

    public static GameManager Instance
    {
        get;
        private set;
    }

    public void Awake()
    {
        Instance = this;
    }

    public void RegisterPlayer(Player _player)
    {
        m_player = _player;
    }

    public Player GetPlayer()
    {
        return m_player;
    }


    // Start is called before the first frame update
    void Start()
    {
        Victory += GameVictory;
        Defeat += GameDefeat;
    }

    // Update is called once per frame
    void Update()
    {

        List<QuestData> SelectedQuests = QuestManager.Instance.SelectedQuests;
        bool AllCompleted = true;
        for(int i = 0; i < SelectedQuests.Count; ++i)
        {
            AllCompleted &= SelectedQuests[i].Completed;
        }

        if(m_player.CurrentHealth <= 0)
        {
            if(Defeat != null)
            {
                Defeat();
                Defeat = null;
            }
        }
        else if(AllCompleted)
        {
            if(Victory != null)
            {
                Victory();
                Victory = null;
            }
        }

        AliveTime += Time.deltaTime;
    }

    public void AddZombie()
    {
        NumZombies++;
    }

    public void RemoveZombie()
    {
        NumZombies--;
        NumZombiesKilled++;
    }

    public void GameVictory()
    {
        if(PlayerPrefs.GetInt("NumZombiesKilled") < NumZombiesKilled)
             PlayerPrefs.SetInt("NumZombiesKilled", NumZombiesKilled);
        if(PlayerPrefs.GetFloat("AliveTime") < AliveTime)
            PlayerPrefs.SetFloat("AliveTime", AliveTime);
        if(PlayerPrefs.GetFloat("QuestTime") > AliveTime)
            PlayerPrefs.SetFloat("QuestTime", AliveTime);

        PlayerPrefs.Save();

        UIManager.Instance.ShowWinUI();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void GameDefeat()
    {
        if(PlayerPrefs.GetInt("NumZombiesKilled") < NumZombiesKilled)
             PlayerPrefs.SetInt("NumZombiesKilled", NumZombiesKilled);
        if(PlayerPrefs.GetFloat("AliveTime") < AliveTime)
            PlayerPrefs.SetFloat("AliveTime", AliveTime);

        PlayerPrefs.Save();

        UIManager.Instance.ShowDefeatUI();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
