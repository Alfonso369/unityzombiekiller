using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public delegate void OnPress();
    public delegate void OnSwitch(int _nextWeapon);
    public delegate void OnMovement(float _horizontalAxis, float _verticalAxis, bool _isRunning, bool _isCrouch);
    public delegate void OnCamera(float _mouseXAxis, float _mouseYAxis);

    public event OnSwitch OnWeaponSwitch;
    public event OnPress OnPlayerShooting;
    public event OnPress OnPlayerStopShooting;
    public event OnPress OnWeaponReload;

    public event OnMovement OnMovementInput;
    public event OnCamera OnCameraInput;
    public event OnPress OnPlayerJump;
    public event OnPress OnPlayerCrouch;
    public event OnPress OnPlayerStandUp;

    [Header("Weapon Key Bindings")]
    public KeyCode[] WeaponSwitchingKeys;
    public KeyCode WeaponShootingKey;
    public KeyCode WeaponReloadKey;

    [Header("Player Movement Key Bindings")]
    public KeyCode PlayerJumpKey;
    public KeyCode PlayerRunningKey;
    public KeyCode PlayerCrouchingKey;

    public static InputManager Instance
    {
        get;
        private set;
    }

    public void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Input for weapon switching
        for (int i = 0; i < WeaponSwitchingKeys.Length; ++i)
        {
            if (Input.GetKeyDown(WeaponSwitchingKeys[i]))
            {
                if(OnWeaponSwitch != null)
                {
                    OnWeaponSwitch(i);
                }
            }
        }

        //Input for weapon shooting
        if(Input.GetKey(WeaponShootingKey))
        {
            if(OnPlayerShooting != null)
            {
                OnPlayerShooting();
            }
        }
        else if(Input.GetKeyUp(WeaponShootingKey))
        {
            if(OnPlayerStopShooting != null)
            {
                OnPlayerStopShooting();
            }
        }

        //Input for weapon reload
        if(Input.GetKeyDown(WeaponReloadKey))
        {
            if(OnWeaponReload != null)
            {
                OnWeaponReload();
            }
        }
        //Input for player Crouch
        if(Input.GetKeyDown(PlayerCrouchingKey))
        {
            if (OnPlayerCrouch != null)
            {
                OnPlayerCrouch();
            }
        }
        else if(Input.GetKeyUp(PlayerCrouchingKey))
        {
            if(OnPlayerStandUp != null)
            {
                OnPlayerStandUp();
            }
        }

        //Input for player movement
        if(OnMovementInput != null)
        {
            OnMovementInput(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), Input.GetKey(PlayerRunningKey), Input.GetKey(PlayerCrouchingKey));
        }

        //Input for player jump
        if(Input.GetKeyDown(PlayerJumpKey))
        {
            if(OnPlayerJump != null)
            {
                OnPlayerJump();
            }
        }

        //Input for camera movement
        if(OnCameraInput != null)
        {
            OnCameraInput(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        }

    }
}
