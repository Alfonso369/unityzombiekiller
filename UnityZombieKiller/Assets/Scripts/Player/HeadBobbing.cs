using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadBobbing : MonoBehaviour
{
    public float WalkingBobbingSpeed = 14f;
    public float BobbingAmount = 0.05f;
    public FPSCharacterController Controller;

    float m_defaultPosY = 0;
    float m_timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.Defeat += Death;
        m_defaultPosY = transform.localPosition.y;
    }

    // Update is called once per frame
    void Update()
    {

        if (Mathf.Abs(Controller.Controller.velocity.x) > 0.1f || Mathf.Abs(Controller.Controller.velocity.z) > 0.1f)
        {
            //Player is moving
            m_timer += Time.deltaTime * WalkingBobbingSpeed;
            transform.localPosition = new Vector3(transform.localPosition.x, m_defaultPosY + Mathf.Sin(m_timer) * BobbingAmount, transform.localPosition.z);
        }
        else
        {
            //Idle
            m_timer = 0;
            transform.localPosition = new Vector3(transform.localPosition.x, Mathf.Lerp(transform.localPosition.y, m_defaultPosY, Time.deltaTime * WalkingBobbingSpeed), transform.localPosition.z);
        }

    }

    private void Death()
    {
        enabled = false;
    }
}
