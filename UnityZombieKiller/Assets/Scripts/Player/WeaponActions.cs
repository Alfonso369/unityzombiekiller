using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponActions : MonoBehaviour
{
    public Player Player;
    public VerySimplePistol[] Weapons;

    private int m_currentWeapon = 0;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.Defeat += Death;
        Player = GameManager.Instance.GetPlayer();
        InputManager.Instance.OnWeaponSwitch += WeaponSwitch;
        InputManager.Instance.OnPlayerShooting += WeaponShoot;
        InputManager.Instance.OnPlayerStopShooting += WeaponStopShoot;
        InputManager.Instance.OnWeaponReload += WeaponReload;
    }

    private void Death()
    {
       enabled = false;
    }

    void WeaponSwitch(int _nextWeapon)
    {
        if(_nextWeapon < Weapons.Length && _nextWeapon != m_currentWeapon)
        {
            Weapons[m_currentWeapon].SetEnable(false);
            Weapons[_nextWeapon].SetEnable(true);
            m_currentWeapon = _nextWeapon;
        }
    }

    void WeaponShoot()
    {
        Weapons[m_currentWeapon].WeaponTriggerPressed();
    }

    void WeaponStopShoot()
    {
        Weapons[m_currentWeapon].WeaponTriggerReleased();
    }

    void WeaponReload()
    {
        Weapons[m_currentWeapon].Reload();
    }

    public VerySimplePistol GetCurrentWeapon()
    {
        return Weapons[m_currentWeapon];
    }

    private void OnDisable()
    {
        InputManager.Instance.OnWeaponSwitch -= WeaponSwitch;
        InputManager.Instance.OnPlayerShooting -= WeaponShoot;
        InputManager.Instance.OnPlayerStopShooting -= WeaponStopShoot;
        InputManager.Instance.OnWeaponReload -= WeaponReload;
    }
}
