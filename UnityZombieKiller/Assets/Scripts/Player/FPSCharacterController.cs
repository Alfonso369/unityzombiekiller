using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]

public class FPSCharacterController : MonoBehaviour
{
    public CharacterController Controller;
    public Player Player;
    private float m_verticalVelocity;
    private float m_isGroundedTimer;
    public float m_walkSpeed = 2.0f;
    public float m_runSpeed = 4.0f;
    public float m_crouchSpeed = 1.0f;
    public float m_jumpHeight = 1.0f;
    private float m_gravity = 9.81f;
    private Vector2 m_playerCameraRotation = Vector2.zero;
    public float PlayerCameraRotationSpeed = 2.0f;
    public float PlayerCameraRotationXLimit = 45.0f;
    public float StaminaToUse = 10f;
    public Camera PlayerCamera;
    public Rigidbody Rigidbody;
    public Animation DeadAnimation;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.Defeat += Death;
        Player = GameManager.Instance.GetPlayer();
        Controller = gameObject.GetComponent<CharacterController>();
        DeadAnimation = gameObject.GetComponent<Animation>();
        InputManager.Instance.OnMovementInput += Move;
        InputManager.Instance.OnCameraInput += CameraRotation;
        InputManager.Instance.OnPlayerJump += Jump;
        InputManager.Instance.OnPlayerCrouch += Crouch;
        InputManager.Instance.OnPlayerStandUp += StandUp;
    }

    // Update is called once per frame
    void Update()
    {
        bool groundedPlayer = Controller.isGrounded;
        if (groundedPlayer)
        {
            m_isGroundedTimer = 0.2f;
        }

        if (m_isGroundedTimer > 0)
        {
            m_isGroundedTimer -= Time.deltaTime;
        }

        if (groundedPlayer && m_verticalVelocity < 0)
        {
            m_verticalVelocity = 0f;
        }

        m_verticalVelocity -= m_gravity * Time.deltaTime;
    }

    void Move(float _horizontalAxis, float _verticalAxis, bool _isRunning, bool _isCrouch)
    {
        Vector3 move = transform.right * _horizontalAxis + transform.forward * _verticalAxis;

        if(_isCrouch)
            move *= m_crouchSpeed;
        else if(_isRunning && Player.CurrentStamina > 0)
        {
            move *= m_runSpeed;
            Player.SubstractStamina(StaminaToUse * Time.deltaTime);
        }
        else
            move *= m_walkSpeed;

        move.y = m_verticalVelocity;
        Controller.Move(move * Time.deltaTime);
    }

    void CameraRotation(float _mouseXAxis, float _mouseYAxis)
    {
        m_playerCameraRotation.y += _mouseXAxis * PlayerCameraRotationSpeed;
        m_playerCameraRotation.x += -_mouseYAxis * PlayerCameraRotationSpeed;
        m_playerCameraRotation.x = Mathf.Clamp(m_playerCameraRotation.x, -PlayerCameraRotationXLimit, PlayerCameraRotationXLimit);
        PlayerCamera.transform.localRotation = Quaternion.Euler(m_playerCameraRotation.x, 0, 0);
        transform.eulerAngles = new Vector2(0, m_playerCameraRotation.y);
    }

    void Jump()
    {
        if (m_isGroundedTimer > 0)
        {
            m_isGroundedTimer = 0;

            m_verticalVelocity += Mathf.Sqrt(m_jumpHeight * 2 * m_gravity);
        }
    }

    void Crouch()
    {
        Controller.height /= 2;
    }

    void StandUp()
    {
        Controller.height *= 2;
    }

    void Death()
    {
        InputManager.Instance.OnMovementInput -= Move;
        InputManager.Instance.OnCameraInput -= CameraRotation;
        InputManager.Instance.OnPlayerJump -= Jump;
        InputManager.Instance.OnPlayerCrouch -= Crouch;
        InputManager.Instance.OnPlayerStandUp -= StandUp;
        
        DeadAnimation.Play();
        Rigidbody.isKinematic = true;
        //Player Death Anim
    }
}
