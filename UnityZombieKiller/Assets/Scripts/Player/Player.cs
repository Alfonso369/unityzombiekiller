using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject ItemEffectsSpot;
    
    public float MaxHealth = 100f;
    public float CurrentHealth = 100f;
    public float MaxStamina = 100f;
    public float CurrentStamina = 100f;

    private float m_timer = 0;
    public float StaminaAmountTick;
    public float TimeToRegenerate;

    public void Awake()
    {
       GameManager.Instance.RegisterPlayer(this);
    }

    private void Update()
    {
        m_timer += Time.deltaTime;

        if(m_timer >= TimeToRegenerate)
        {
            RegenerateStamina();
        }
    }

    public void OnTriggerEnter(Collider _other)
    {
        _other.GetComponent<TriggerObject>().OnTriggerWithPlayer(this);
    }

    public VerySimplePistol GetCurrentWeapon()
    {
        return GetComponent<WeaponActions>().GetCurrentWeapon();
    }

    public void AddAmmo(PistolType _ammoType, int _ammo)
    {
        WeaponActions weapons = GetComponent<WeaponActions>();

        for(int i = 0; i < weapons.Weapons.Length; i++)
        {
            if(weapons.Weapons[i].Type == _ammoType)
            {
                weapons.Weapons[i].AddAmmo(_ammo);
                break;
            }
        }
    }

    public void AddHealth(float _health)
    {
        CurrentHealth = Mathf.Clamp(CurrentHealth + Mathf.Abs(_health), 0, MaxHealth);
    }

    public void TakeDamage(float _damage)
    {
        CurrentHealth = Mathf.Clamp(CurrentHealth - Mathf.Abs(_damage), 0, MaxHealth);
    }

    public void SubstractStamina(float _amount)
    {
        CurrentStamina = Mathf.Clamp(CurrentStamina - Mathf.Abs(_amount), 0, MaxStamina);
        m_timer = 0;
    }

    private void RegenerateStamina()
    {
        CurrentStamina = Mathf.Clamp(CurrentStamina + StaminaAmountTick * Time.deltaTime, 0, MaxStamina);
    }
}
