using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class MainMenuController : MonoBehaviour
{

    public GameObject MainMenu;
    public GameObject ScoreboardMenu;
    public GameObject LoadPanel;
    public Slider LoadingBar;
    public GameObject FinishText;

    public TextMeshProUGUI NumZombiesScoreText;
    public TextMeshProUGUI AliveTimeScoreText;
    public TextMeshProUGUI QuestTimeScoreText;

    private void Start()
    {
        LoadSavedInfo();
    }
    public void OnPlay()
    {
        MainMenu.SetActive(false);
        LoadPanel.SetActive(true);
        StartCoroutine(LoadAsynchronously("Scene_A"));
        //Sound
    }

    IEnumerator LoadAsynchronously(string sceneName) // scene name is just the name of the current scene being loaded
	{ 
		AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
		operation.allowSceneActivation = false;

		while (!operation.isDone)
		{
			float progress = Mathf.Clamp01(operation.progress / .9f);
			LoadingBar.value = progress;

			if(operation.progress >= 0.9f)
			{
				FinishText.gameObject.SetActive(true);

				if(Input.anyKeyDown)
				{
					operation.allowSceneActivation = true;
				}
			}
				
			yield return null;
		}
	}

    public void OnScoreboard()
    {
        MainMenu.SetActive(false);
        ScoreboardMenu.SetActive(true);
        //Sound
    }

    public void OnExit()
    {
        //Sound
        Application.Quit();
    }

    public void OnBack()
    {
        ScoreboardMenu.SetActive(false);
        MainMenu.SetActive(true);
        //Sound
    }

    private void LoadSavedInfo()
    {
        if(PlayerPrefs.HasKey("NumZombiesKilled"))
            NumZombiesScoreText.SetText(PlayerPrefs.GetInt("NumZombiesKilled").ToString());
        else
        {
            NumZombiesScoreText.SetText("0");
            PlayerPrefs.SetInt("NumZombiesKilled", 0);
        }

        if(PlayerPrefs.HasKey("AliveTime"))
            AliveTimeScoreText.SetText(PlayerPrefs.GetFloat("AliveTime").ToString("F2"));
        else
        {
            AliveTimeScoreText.SetText("0.0");
            PlayerPrefs.SetFloat("AliveTime", 0.0f);
        }

        if(PlayerPrefs.HasKey("QuestTime"))
        {
           if(PlayerPrefs.GetFloat("QuestTime") < float.MaxValue)
                QuestTimeScoreText.SetText(PlayerPrefs.GetFloat("QuestTime").ToString("F2"));
           else
                QuestTimeScoreText.SetText("NOPE");
        }
        else
        {
            QuestTimeScoreText.SetText("NOPE");
            PlayerPrefs.SetFloat("QuestTime", float.MaxValue);
        }

         PlayerPrefs.Save();
        
    }
}
